<?php
header('Content-Type: application/json');
define("baseUrl", "https://jsonplaceholder.typicode.com/");

$userId = isset($_GET['id']) ? $_GET['id'] : die();

getAllByUser($userId);

function create_request($_url)
{
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $_url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    curl_close($ch);      
    return $output;
}

function getAllAlbunsByUser($_userId)
{
    return create_request(baseUrl . "albums?userId=" . $_userId); 
}

function getAllByUser($_userId)
{
    $albuns = json_decode(getAllAlbunsByUser($_userId));
    $photosURL = [];

    foreach($albuns as $album)
    {
        $url = baseUrl . "photos?albumId=" . $album->id;
        $photosByAlbum = create_request($url);
        foreach(json_decode($photosByAlbum) as $photo) 
        {
            $photosURL[] = $photo->url;
        }
    }

    echo json_encode($photosURL);
}
